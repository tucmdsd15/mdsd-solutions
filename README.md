# MDSD Solutions
---
## Status:

* E1 ✓ [Solution](https://docs.google.com/document/d/1WmRXEu34u-MtyCw6EQipV29RiCeL86dWnkmFDPuz584/edit#heading=h.twjpf3j1sl0s "Solution E1")
* E2 ✓ [Solution](https://docs.google.com/document/d/1WmRXEu34u-MtyCw6EQipV29RiCeL86dWnkmFDPuz584/edit#heading=h.6j20krd0s1pn "Solution E2")
* E3 ✓ [Solution](https://bitbucket.org/tucmdsd15/mdsd-solutions/src/a956e7fb57d3/E3/ "Solution E3") | [Add-on](https://docs.google.com/document/d/1WmRXEu34u-MtyCw6EQipV29RiCeL86dWnkmFDPuz584/edit#heading=h.z3ppr6laxohw "Add-on E3")
* E4 ✓ No exercise, no solution ;)
* E5 ✓ [Solution](https://docs.google.com/document/d/1WmRXEu34u-MtyCw6EQipV29RiCeL86dWnkmFDPuz584/edit#heading=h.h5ybbe1rq4ye "Solution E5")
* E6
* E7
* E8
* E9
* E11
* E12
* E13
* E14
* E15
* E16
* E17
* E18
* E19
* E20
* E21
* E22
* E23 ✓ [Solution](https://bitbucket.org/tucmdsd15/mdsd-solutions/src/a052de42010c/E23/ "Solution E23")
* E24 ✓ See E23
* E25 x See E23 (// TODO: try the "mCRL2 model checker")
* E26 ✓ [Solution](https://bitbucket.org/tucmdsd15/mdsd-solutions/src/7b76226f4afe/E26/ "Solution E26")
* E27 ✓ See E26
* E28
* E29 ✓ [Solution](https://bitbucket.org/tucmdsd15/mdsd-solutions/src/3672dc5bccf5/E29/ "Solution E29")
* E30 ✓ See E29
